-- -----------------------------------------------------
-- RECREATE Schema `vuln`
-- -----------------------------------------------------
DROP DATABASE IF EXISTS vuln;

CREATE DATABASE vuln CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;

-- -----------------------------------------------------
-- User vuln:vuln
-- -----------------------------------------------------
DROP USER IF EXISTS 'vuln'@'%';
CREATE USER 'vuln'@'%' IDENTIFIED BY 'vuln';
GRANT ALL PRIVILEGES ON vuln.* TO 'vuln'@'%';
FLUSH PRIVILEGES;

-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vuln`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL UNIQUE,
  `password` VARCHAR(32) NOT NULL,
  `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_bin;


-- -----------------------------------------------------
-- Table `posts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vuln`.`posts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NULL DEFAULT NULL,
  `body` LONGTEXT NULL DEFAULT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `user_id_idx` (`id` ASC),
  CONSTRAINT `user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `vuln`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_bin;

-- -----------------------------------------------------
-- Seed `users`
-- -----------------------------------------------------

INSERT INTO `vuln`.`users` (`username`, `password`) VALUES ('john@email.com', '2467d3744600858cc9026d5ac6005305');
INSERT INTO `vuln`.`users` (`username`, `password`) VALUES ('jane@example.com', '5f4dcc3b5aa765d61d8327deb882cf99');
INSERT INTO `vuln`.`users` (`username`, `password`) VALUES ('superman@foo.com', 'fc16fc3cea94895ceb0ba213ceea75de');
INSERT INTO `vuln`.`users` (`username`, `password`) VALUES ('bot@example.com', '3efd0f8070d00d486bde2d8369d2e6fc');


-- -----------------------------------------------------
-- Seed `posts`
-- -----------------------------------------------------

INSERT INTO `vuln`.`posts` (`id`, `title`, `body`, `user_id`) VALUES (1, 'Title 1', 'Body 1', 1);
INSERT INTO `vuln`.`posts` (`id`, `title`, `body`, `user_id`) VALUES (2, 'Title 2', 'Body 2', 1);
INSERT INTO `vuln`.`posts` (`id`, `title`, `body`, `user_id`) VALUES (3, 'Title 3', 'body 3', 3);
INSERT INTO `vuln`.`posts` (`id`, `title`, `body`, `user_id`) VALUES (4, 'Title 4', 'Body 4', 2);
INSERT INTO `vuln`.`posts` (`id`, `title`, `body`, `user_id`) VALUES (5, 'Title 5', 'Body 5', 4);