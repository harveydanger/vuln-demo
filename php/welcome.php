<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
} else {
    // DANGER ZONE!!!!
    require_once('config.php');
    $posts = mysqli_query($link,'SELECT * FROM `posts`');    
}

function getUsername($id, $link) {
    $q = mysqli_query($link,'SELECT username from users where id = ' . $id . '');
    
    $username = mysqli_fetch_all($q);
    // var_dump($username[0][0]);
    echo $username[0][0] . '/';
    // echo "1";
}

?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body{ font: 14px sans-serif; text-align: center; }
        .container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
    </style>
</head>
<body>
    <h1 class="my-5">Hi, <b><?php echo htmlspecialchars($_SESSION["username"]); ?></b>. Welcome to our site.</h1>
    <p>
        
        <a href="logout.php" class="btn btn-danger ml-3">Sign Out of Your Account</a>
    </p>
    <a href="create-post.php" class="btn btn-primary">Create Post</a>

    <?php foreach($posts as $post): ?>

        <div class="container p-3">
        <div class="card" style="width:80%">
            <div class="card-header">
                by /<?= getUsername($post['user_id'], $link)?>
            </div>
            <div class="card-body">
                <h5 class="card-title"><?= htmlspecialchars($post['title'])?></h5>
                <p class="card-text"><?= $post['body']?></p>
                <?php if($_SESSION['id'] == $post['user_id']) : ?>
                    <a href="#" class="btn btn-primary">Edit</a>
                <?php endif;?>
                
            </div>
            </div>
        </div>


    <?php endforeach; ?>

</body>
</html>