<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

$title = $message = $error = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
    require_once('config.php');
    $title = $_POST["title"];
    $body = $_POST["message"];
    $id = $_SESSION["id"];
    $prep = 'INSERT INTO `posts` (`title`, `body`, `user_id`) VALUES ("' . $title . '","' . $body . '",' . $id .')';
    $success = mysqli_query($link, $prep);

    if($success) {
        header("location: welcome.php");
        exit;
    } else {
        $error = mysqli_error($link) . ' => ' . $prep;
    }


}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Create Post</title>
    
</head>
<body class="p-5">
    <?php if($error !== "") : ?>
        <div class="alert alert-danger" role="alert">
            <?= $error ?>
        </div>
    <?php endif?>
    <form method="POST">
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" id="title" aria-describedby="titleHelp" placeholder="Enter title" name="title">
        <small id="titleHelp" class="form-text text-muted">Try something original.</small>
    </div>
    <div class="form-group">
        <label for="message">Message</label>
        <input type="text" class="form-control" id="message" placeholder="Tell us what you think" name="message">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</body>
</html>